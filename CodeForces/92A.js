let makeInputNumber = readline ().split (" ")
let input = makeInputNumber.map(val => +val) //convert it to number
const totalAnimal = input [0] //total number of animals
const totalChips = input[1] //total number of chips

// let providedChips = 1
let remainChips = totalChips
let animalCount = 1
let returnChips = 0
let notComplete = true 
while (notComplete) {
    const allocatedChips = animalCount
    if (allocatedChips > remainChips ) {
        returnChips = remainChips
        notComplete = false
        break
    }
    remainChips = remainChips - allocatedChips
    if (animalCount >= totalAnimal) {
        animalCount = 1 
    }else {
        animalCount ++
    }
    
}

console.log(remainChips ) ;