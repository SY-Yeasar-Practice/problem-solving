var makeInputNumber = readline ().split (" ")
let input = makeInputNumber.map(val => +val) //convert it to number

function IsLegAvailable (input) {
    const queryArray = [...input]
    let countLeg = 0 
    let isLeg = false
    let legValue = ""

    for (let i = 0 ; i < queryArray.length / 2 ; i++) {
        if (countLeg != 4) {
            input.forEach((child, ind) => {
                if (queryArray[i] == child) {
                    if (countLeg <= 4) {
                        if (countLeg != 4) {
                            countLeg++
                            if (countLeg == 4) {
                                legValue = queryArray[i]
                                isLeg = true
                            }
                        }
                    }
                }   
            })
        }
        if (countLeg < 4) {
            countLeg = 0
        }
    }
    if (isLeg ) deleteLegValue (input, legValue)
    return {
        isLeg: isLeg,
        legValue: legValue
    }
}

//if leg have found then it will remove all leg value
function deleteLegValue (localInput, legValue) {
    // console.log({localInput, legValue})
    const filterLegValue = localInput.filter (val => val == legValue)
    const withOutLegValue = localInput.filter (val => val != legValue)
    let extraLength = 0
    if (filterLegValue.length > 4) {
        extraLength = filterLegValue.length - 4
    }
    if (extraLength) {
        for (let i = 1; i <= extraLength; i++) {
            withOutLegValue.push(legValue)
        }
    }
    input = [...withOutLegValue]
    return {
        isDelete: true
    }
    
}

const {isLeg, legValue} = IsLegAvailable(input)

if (isLeg) {
    const [inputOne, inputTwo] = input
    if (inputOne > inputTwo || inputTwo > inputOne) console.log(`Bear`)
    else if (inputOne == inputTwo) console.log(`Elephant`)
}else {
    console.log(`Alien`)
}


// =================> Solved <===================================== //