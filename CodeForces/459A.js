const input = readline ().split (" ").map (val => +val)
const x1 = input[0]
const y1 = input [1]
const x2 = input [2]
const y2 = input [3]
const cord = []
if (x1 == x2) {
    const  cordXOne = x1 + y2 - y1
    const cordXTwo =  x2 + y2 - y1
    cord.push (cordXOne, y1, cordXTwo, y2)
}else if (y1 == y2) {
    const cordYOne = y1 + x2 - x1
    const cordYTwo = y2 + x2 - x1
    cord.push (x1, cordYOne, x2, cordYTwo)
}else if (Math.abs (x1 - x2) == Math.abs (y1 - y2)) {
    cord.push (x1,y2,x2,y1)
}else {
    cord.push (-1)
}
cord.forEach (val => console.log(val))